﻿(function () {
    'use strict';

    describe("String Extensions", function () {

        describe("Capitalise", function () {

            it('should capitalise only first character', function () {
                expect(StringUtils.capitalise("original string"))
                    .toBe("Original string");
            });

            it('should handle null and empty values', function () {
                var s = new String();

                expect(StringUtils.capitalise(s))
                    .toEqual(s);

                expect(StringUtils.capitalise(''))
                    .toBe('');
            });
        });


        describe('camelCaseToSpineCase', function () {

            it('should produce the correct result', function () {
                expect(StringUtils.camelCaseToSpineCase("someCamelCase"))
                    .toBe("some-camel-case");
            });

            it('should handle whitespace', function () {
                expect(StringUtils.camelCaseToSpineCase('some-CamelCase with Space'))
                    .toBe('some-camel-case-with-space');
            });

            it('should handle digits', function () {
                expect(StringUtils.camelCaseToSpineCase("s0m3C4m31C4s3"))
                    .toBe("s0m3-c4m31-c4s3");
            });

            it('should handle null and empty values', function () {
                var s = new String();
                expect(StringUtils.camelCaseToSpineCase(s)).toEqual(s);
                expect(StringUtils.camelCaseToSpineCase('')).toBe('');
            });
        });

        describe("spineCaseToCamelCase", function () {

            it('should convert snake case to camelcase', function () {
                expect(StringUtils.spineCaseToCamelCase("some-snake-Case"))
                    .toBe("someSnakeCase");
            });

            it('should handle whitespace', function () {
                expect(StringUtils.spineCaseToCamelCase('  some snakeCase with Space '))
                    .toBe('someSnakeCaseWithSpace');
            });

            it('should handle digits', function () {
                expect(StringUtils.spineCaseToCamelCase("s0m3-5n4k3-C4s3"))
                    .toBe("s0m35n4k3C4s3");
            });

            it('should handle null and empty values', function () {
                var s = new String();
                expect(StringUtils.spineCaseToCamelCase(s))
                    .toEqual(s);
                expect(StringUtils.spineCaseToCamelCase(""))
                    .toBe("");
            });
        });

        describe("format", function () {

            it('should handle a string with no arguments', function () {
                expect(StringUtils.format('string without any arguments'))
                    .toBe('string without any arguments');
            });

            it('should handle a string with only one argument', function () {
                expect(StringUtils.format("string with {0} argument", "one"))
                    .toBe("string with one argument");
            });

            it('should handle a string with more than one argument', function () {
                expect(StringUtils.format('string with {0}, {1}, {2} or more arguments', "one", "two", "three"))
                    .toBe('string with one, two, three or more arguments');
            });

            it('should handle a number as an argument', function () {
                expect(StringUtils.format('string with the number {0} in it.', 1))
                    .toBe('string with the number 1 in it.');
            });

            it('should handle null and empty values', function () {
                var s = new String();
                expect(StringUtils.format(s, "test"))
                    .toEqual(s);

                expect(StringUtils.format("", "test"))
                    .toBe('');
            });
        });
    });

})();
