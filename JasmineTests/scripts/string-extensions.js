﻿"use strict";

var StringUtils = (function (module) {

    module.isEmpty = function(str) {
        return (!str || str.length === 0);
    }

    module.capitalise = function (str) {
        if (module.isEmpty(str))
            return "";

        var result = (str.substring(0, 1).toUpperCase()) + str.substring(1);
        return result;
    }

    module.camelCaseToSpineCase = function (str) {
        if (module.isEmpty(str))
            return "";

        var result = str
            .replace(/(\s)/g, "-")
            .replace(/([a-z0-9])([A-Z|\s])/g, "$1-$2")
            .toLowerCase();

        return result;
    }

    module.spineCaseToCamelCase = function(str) {
        if (module.isEmpty(str))
            return "";

        var result = str
            .trim()
            .replace(/(\s)/g, "-")
            .replace(/(-)([a-zA-Z0-9])|\s/g, function (match, $1, $2) {
                return $2.toUpperCase();
            });
        
        return result;
    }

    module.format = function () {
        var args = Array.from(arguments);
        var str = args.splice(0, 1)[0];

        var index = 0;
        args.forEach(function (arg) {
            str = str.replace(new RegExp("{(" + index + ")}"), arg);
            index++;
        });

        return str;
    }

    return module;
})(StringUtils || {});
