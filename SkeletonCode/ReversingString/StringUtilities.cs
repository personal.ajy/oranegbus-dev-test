﻿using System;
using System.Diagnostics;

namespace SkeletonCode.ReversingString
{
	public class StringUtilities
	{
        /// <summary>
        /// The original method creates a new string instance every iteration before
        /// appending it to the output string The below algorithm creates a new character
        /// array of the input string's size and is recursively updated with the input's
        /// characters in the reverse order. When benchmarking both examples, the method
        /// below is marginally faster.
        /// </summary>
        public string Reverse(string input)
        {
            // Treat null values as empty/whitespace strings
            // No need to reverse empty strings
            if (string.IsNullOrWhiteSpace(input))
                return "";

            // Length of string minus 1, for iteration 
            var iCount = input.Length - 1;
            // Create empty char array for storing characters
            var array = new char[input.Length];
            for (var i = 0; i <= iCount; i++)
            {
                // Recursively get each character from the input string
                // and update the result array
                array[i] = input[iCount - i];
            }

            return new string(array);
        }
    }
}
