﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SkeletonCode.CardGame.Enums;

namespace SkeletonCode.CardGame
{
	public class PackOfCardsCreator : IPackOfCardsCreator
	{
		public IPackOfCards Create()
		{
            var suits = Enum.GetValues(typeof(Suit));
            var ranks = Enum.GetValues(typeof(Rank));

            var cards = new Collection<ICard>();
            foreach (Suit suit in suits)
            {
                foreach (Rank rank in ranks)
                {
                    var card = new Card(suit, rank);
                    cards.Add(card);
                }
            }

            var pack = new PackOfCards(cards);
            return pack;
		}
	}
}
