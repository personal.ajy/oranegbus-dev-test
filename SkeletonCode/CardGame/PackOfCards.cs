﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonCode.CardGame
{
    public class PackOfCards : IPackOfCards
    {
        private ICollection<ICard> _cards;

        public int Count
        {
            get
            {
                return _cards?.Count ?? 0;
            }
        }

        #region Implementation

        public IEnumerator<ICard> GetEnumerator()
        {
            return _cards.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        public PackOfCards(ICollection<ICard> cards)
        {
            _cards = cards;
        }

        #region Functionality

        public void Shuffle()
        {
            var random = new Random();
            // Create Array copy of cards (no need for list)
            var cards = _cards.ToArray();
            // Loop through each card
            for (var i = _cards.Count - 1; i > 0; i--)
            {
                var swapIndex = random.Next(i + 1);

                // Swap current card with card at swap index
                var temp = cards[i];
                cards[i] = cards[swapIndex];
                cards[swapIndex] = temp;
            }

            _cards = cards;
        }

        /// <summary>
        /// This method assumes the "top of the pack" starts from
        /// the end of the collection, as you'd assume when adding a new
        /// item to the collection (a new card to the pack), the item will
        /// be added to the end of the collection.
        /// </summary>
        public ICard TakeCardFromTopOfPack()
        {
            var cardToTake = _cards.Last();
            _cards.Remove(cardToTake);
            return cardToTake;
        }

        #endregion
    }
}
