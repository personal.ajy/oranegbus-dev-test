﻿using System;
using System.Collections.Generic;

namespace SkeletonCode.CurrencyConverter
{
	public class Converter
	{
		public decimal Convert(string inputCurrency, string outputCurrency, decimal amount)
		{
            // Store currency values based on test specification
            var currencies = new Dictionary<string, decimal>
            {
                {"USD", 1.25m },
                {"GBP", 1m }
            };

            // Ensure the given currency is valid
            if (!currencies.ContainsKey(inputCurrency) || !currencies.ContainsKey(outputCurrency))
                throw new InvalidOperationException("Invalid currency given.");

            // Get currency values
		    var inputCurrVal = currencies[inputCurrency];
		    var outputCurrVal = currencies[outputCurrency];

            // Calculate converted amount value
            var conversionTotal = (amount / inputCurrVal) * outputCurrVal;

		    return conversionTotal;
		}
	}
}
